drop table Allergene CASCADE CONSTRAINTS;
drop table Speisekarte CASCADE CONSTRAINTS;
drop table Makronährstoffe CASCADE CONSTRAINTS;
drop table Gericht CASCADE CONSTRAINTS;
drop table GerichtAllergene CASCADE CONSTRAINTS;
drop table Owner CASCADE CONSTRAINTS;
drop table Betrieb CASCADE CONSTRAINTS;

create table Allergene(
  allergenID int PRIMARY KEY,
  bezeichnung VARCHAR2(60)
);

create table Speisekarte (
  speisekartenID INT PRIMARY KEY
);

create table Makronährstoffe (
  makronährstoffeID int PRIMARY KEY,
  fette int,
  kohlenhydrate int,
  proteine int
);
create table Gericht(
  gerichtID int primary key,
  bezeichnung VARCHAR2(80),
  preis int,
  makronährstoffe int,
  speisekartenID int,
  FOREIGN KEY(makronährstoffe) REFERENCES  Makronährstoffe(makronährstoffeID),
  FOREIGN KEY(speisekartenID) REFERENCES Speisekarte(speisekartenID)
);

create table GerichtAllergene (
  gerichtID INT,
  allergenID INT,
  PRIMARY KEY(gerichtID,allergenID),
  FOREIGN KEY(gerichtID) REFERENCES Gericht(gerichtID),
  FOREIGN KEY(allergenID) REFERENCES Allergene(allergenID)
);

create table Owner(
  ownerID int PRIMARY KEY,
  name VARCHAR2(100),
  ssnr VARCHAR2(48)
);

create table Betrieb(
  betriebID int PRIMARY KEY,
  lat varchar2(10),
  lng varchar2(10),
  name VARCHAR2(140),
  speisekarte int,
  owner int,
  FOREIGN KEY(speisekarte) REFERENCES Speisekarte(speisekartenID),
  FOREIGN KEY(owner) REFERENCES Owner(ownerID)
);