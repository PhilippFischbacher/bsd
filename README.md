<h1>Sprint Board unter: </h1>
https://gitlab.com/PhilippFischbacher/bsd/boards/853983

Hiermit wird der Fortschritt und die Aufgabenteilung dargestellt.


<h2>Commits pro Person:</h2>
https://gitlab.com/PhilippFischbacher/bsd/graphs/master

<h1>Repository auschecken</h1>
    
<h2>SSH (empfohlen, setzt SSH Key auf eigenem Rechner voraus)</h2>
        
        git clone git@gitlab.com:PhilippFischbacher/bsd.git
        
<h2>HTTPS</h2>
        
        git clone https://gitlab.com/PhilippFischbacher/bsd.git
        
    