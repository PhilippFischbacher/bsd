package dbox.bxx.gastro_mobiletool;

import com.google.android.gms.maps.model.LatLng;

public class SimpleMarker {

    private int id;
    private LatLng latLng;
    private String title;

    public SimpleMarker(int id, LatLng latLng, String title) {
        this.id = id;
        this.latLng = latLng;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}