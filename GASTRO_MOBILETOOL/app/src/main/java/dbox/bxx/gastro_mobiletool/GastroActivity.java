package dbox.bxx.gastro_mobiletool;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GastroActivity extends FragmentActivity implements OnMapReadyCallback {

    private String GETRESTAURANTURL;
    private Intent thisIntent;
    private String IP;
    private int gastroID;
    String businessName;
    private GoogleMap mMap;
    private MapView mapView;

    private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gastro);

        thisIntent = getIntent();
        IP = thisIntent.getStringExtra("ip");
        businessName = thisIntent.getStringExtra("gastroName");
        gastroID = thisIntent.getIntExtra("gastroID", -1);
        GETRESTAURANTURL = "http://" + IP + ":8080/BSD/rest/web/restaurant/" + gastroID;

        TextView txt = (TextView) findViewById(R.id.txtBusinessName);
        txt.setText(businessName);


        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }

        mapView = findViewById(R.id.mapView);
        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(this);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Use the Builder class for convenient dialog construction
                AlertDialog.Builder builder = new AlertDialog.Builder(GastroActivity.this);
                builder.setTitle("Speisekarte");
                builder.setMessage("Döner: 3,50€\nDürüm: 4,80€\nKebaprolle: 3,30€");
                // Create the AlertDialog object and return it
                builder.create().show();
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }

        mapView.onSaveInstanceState(mapViewBundle);
    }

    private void RequestRestaurantMarker() {
        final RequestQueue requestQueue = Volley.newRequestQueue(this);

        // Initialize a new JsonObjectRequest instance
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                GETRESTAURANTURL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        JSONObject e = response;

                        try {
                            int id = e.getInt("betriebID");
                            int dishlistID = e.getInt("speisekarte");
                            Double lat = e.getDouble("lat");
                            Double lng = e.getDouble("lng");
                            String name = e.getString("name");
                            int ownerID = e.getInt("owner");

                            refreshView(new GastroBusiness(id, name, lat, lng, dishlistID, ownerID));

                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(GastroActivity.this, "TimeOutError: " + error.toString(), Toast.LENGTH_SHORT).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(GastroActivity.this, "AuthFailureError: " + error.toString(), Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(GastroActivity.this, "ServerError: " + error.toString(), Toast.LENGTH_SHORT).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(GastroActivity.this, "NetworkError: " + error.toString(), Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(GastroActivity.this, "ParseError: " + error.toString(), Toast.LENGTH_SHORT).show();
                        }
                        */
                        Toast.makeText(GastroActivity.this, "Cannot find Business with ID: " + gastroID, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        // Add JsonObjectRequest to the RequestQueue
        requestQueue.add(jsonObjectRequest);
    }

    private void refreshView(GastroBusiness gastroBusiness) {
        mMap.addMarker(new MarkerOptions().position(new LatLng(gastroBusiness.getLat(), gastroBusiness.getLng())).title(gastroBusiness.getName()));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        RequestRestaurantMarker();
    }
}
