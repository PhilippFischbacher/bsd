package dbox.bxx.gastro_mobiletool;

public class GastroBusiness {

    private int id;
    private String name;
    private double lat;
    private double lng;
    private int dishlistID;
    private int ownerID;

    public GastroBusiness(int id, String name, double lat, double lng, int dishlistID, int ownerID) {
        this.id = id;
        this.name = name;
        this.lat = lat;
        this.lng = lng;
        this.dishlistID = dishlistID;
        this.ownerID = ownerID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public int getDishlistID() {
        return dishlistID;
    }

    public void setDishlistID(int dishlistID) {
        this.dishlistID = dishlistID;
    }

    public int getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(int ownerID) {
        this.ownerID = ownerID;
    }
}
