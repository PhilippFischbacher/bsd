package dbox.bxx.gastro_mobiletool;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    private GoogleMap mMap;
    //private String IP = getString(R.string.IP);
    private String GETALLRESTAURANTSURL;
    private Intent thisIntent;
    private String IP;
    private HashMap<Marker, SimpleMarker> allGastro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        thisIntent = getIntent();
        IP = thisIntent.getStringExtra("ip");
        GETALLRESTAURANTSURL = "http://" + IP + ":8080/BSD/rest/web/restaurant";
        allGastro = new HashMap<>();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnInfoWindowClickListener(this);
        RequestAllRestaurantMarkers();
    }

    private void RequestAllRestaurantMarkers() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        // Initialize a new JsonObjectRequest instance
        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(
                Request.Method.GET,
                GETALLRESTAURANTSURL,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        // Do something with response
                        //mTextView.setText(response.toString());
                        ArrayList<SimpleMarker> allItems = new ArrayList<>();

                        // Process the JSON
                        try {
                            // Get the JSON array
                            JSONArray array;
                            array = response;

                            // Loop through the array elements
                            for (int i = 0; i < array.length(); i++) {
                                // Get current json object
                                JSONObject e = array.getJSONObject(i);

                                // Get the current student (json object) data
                                int id = e.getInt("betriebID");
                                Double lat = e.getDouble("lat");
                                Double lng = e.getDouble("lng");
                                String name = e.getString("name");

                                allItems.add(new SimpleMarker(id, new LatLng(lat, lng), name));
                            }

                            SetMarkers(allItems);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(MapsActivity.this, "TimeOutError: " + error.toString(), Toast.LENGTH_SHORT).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(MapsActivity.this, "AuthFailureError: " + error.toString(), Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(MapsActivity.this, "ServerError: " + error.toString(), Toast.LENGTH_SHORT).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(MapsActivity.this, "NetworkError: " + error.toString(), Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(MapsActivity.this, "ParseError: " + error.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        );

        // Add JsonObjectRequest to the RequestQueue
        requestQueue.add(jsonObjectRequest);
    }

    private void SetMarkers(List<SimpleMarker> allMarkers) {

        for (SimpleMarker m : allMarkers) {
            Marker marker = mMap.addMarker(new MarkerOptions().position(m.getLatLng()).title(m.getTitle()));
            marker.setTag(m.getId());
        }

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(allMarkers.get(0).getLatLng(), 16.0f));
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        String gastroName = marker.getTitle();
        int gastroID = (int) marker.getTag();

        //Toast.makeText(this, "Tag: " + marker.getTag(), Toast.LENGTH_SHORT).show();
        Intent myIntent = new Intent(MapsActivity.this, GastroActivity.class);
        myIntent.putExtra("ip", IP);
        myIntent.putExtra("gastroID", gastroID);
        myIntent.putExtra("gastroName", gastroName);
        MapsActivity.this.startActivity(myIntent);
    }
}
