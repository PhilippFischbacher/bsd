#!/bin/bash
binPath="./bin"
srcPath="./src"
libPath="./lib"
warName="BSD"
apachePath="/opt/apache-tomcat"

rm -rf $libPath
mkdir -p $libPath

cp -R $warName/WebContent $libPath/tmp
cp -R $warName/build/classes $libPath/tmp/WEB-INF/

cd $libPath/tmp
jar -cvf $warName.war .

mv $warName.war ../../lib/

cd ..
cd ..

rm -rf $libPath/tmp

cp $libPath/$warName.war $apachePath/webapps
cp oracleextern.conf /opt/apache-tomcat/webapps/oracleextern.conf
sh /opt/apache-tomcat-9.0.4/bin/startup.sh
