package DomainClasses;

import java.util.ArrayList;

public class Speisekarte {

	private int ID;
	private ArrayList<Gericht> gerichts= new ArrayList<Gericht>(); 
	
	

	public ArrayList<Gericht> getGerichts() {
		return gerichts;
	}

	public void setGerichts(ArrayList<Gericht> gerichts) {
		this.gerichts = gerichts;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}
	
	public String toJson() {
		String CompJSON = "{";
		String newline = System.getProperty("line.separator");
		CompJSON +="\"ID\": " + this.ID + ",";
		CompJSON += newline;
		CompJSON += "\"Gerichte\": " + "[";
		for (Gericht gericht : gerichts) {
			CompJSON += gericht.toJson()+","; 
		}
		CompJSON=CompJSON.substring(0, CompJSON.length()-1);
		CompJSON +="]";
		CompJSON += newline;
		CompJSON +="}";
		return CompJSON;
	}
	
}
