package DomainClasses;

public class Gericht {

	private int ID;
	private int Preis;
	private String bezeichnung;
	private int makronaehrstoffe ;
	private int speisekartenID ;
	
	
	
	public int getPreis() {
		return Preis;
	}
	public void setPreis(int preis) {
		Preis = preis;
	}
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public int getMakronaehrstoffe() {
		return makronaehrstoffe;
	}
	public void setMakronaehrstoffe(int makronaehrstoffe) {
		this.makronaehrstoffe = makronaehrstoffe;
	}
	public int getSpeisekartenID() {
		return speisekartenID;
	}
	public void setSpeisekartenID(int speisekartenID) {
		this.speisekartenID = speisekartenID;
	}
	
	public String toJson() {
		String CompJSON = "{";
		String newline = System.getProperty("line.separator");
		CompJSON +="\"ID\": " + this.ID + ",";
		CompJSON += newline;
		CompJSON +="\"Preis\": " + this.Preis + ",";
		CompJSON += newline;
		CompJSON +="\"bezeichnung\": \"" + this.bezeichnung + "\",";
		CompJSON += newline;
		CompJSON +="\"makronaehrstoffe\": " + this.makronaehrstoffe + "";
		CompJSON += newline;
		CompJSON +="}";
		
		return CompJSON;
	}
	
}
