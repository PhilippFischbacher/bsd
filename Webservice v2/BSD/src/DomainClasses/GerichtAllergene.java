package DomainClasses;

public class GerichtAllergene {

	private int gerichtID;
	private int allergeneID;
	
	public int getGerichtID() {
		return gerichtID;
	}
	public void setGerichtID(int gerichtID) {
		this.gerichtID = gerichtID;
	}
	public int getAllergeneID() {
		return allergeneID;
	}
	public void setAllergeneID(int allergeneID) {
		this.allergeneID = allergeneID;
	}
	
}
