package DomainClasses;

public class Betrieb {

	private int betriebID;
	private String name;
	private int speisekarte;
	private int owner;
	private String lat;
	private String lng;
	
	
	
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLng() {
		return lng;
	}
	public void setLng(String lng) {
		this.lng = lng;
	}
	public void setBetriebID(int betriebID) {
		this.betriebID = betriebID;
	}
	public void setOwner(int owner) {
		this.owner = owner;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSpeisekarte() {
		return speisekarte;
	}
	public void setSpeisekarte(int speisekarte) {
		this.speisekarte = speisekarte;
	}
	public int getBetriebID() {
		return betriebID;
	}
	public int getOwner() {
		return owner;
	}
	
	public String toJson() {
		String CompJSON = "{";
		String newline = System.getProperty("line.separator");
		CompJSON +=newline+ "\"betriebID\": " + this.betriebID + ",";
		CompJSON += newline;
		CompJSON += "\"name\": \"" + this.name + "\",";
		CompJSON += newline;
		CompJSON += "\"lat\": " + this.lat + ",";
		CompJSON += newline;
		CompJSON += "\"lng\": " + this.lng + ",";
		CompJSON += newline;
		CompJSON += "\"speisekarte\": " + this.speisekarte + ",";
		CompJSON += newline;
		CompJSON += "\"owner\": " + this.owner;
		CompJSON += newline;
		CompJSON +="}";
		return CompJSON;
	}
	
}
