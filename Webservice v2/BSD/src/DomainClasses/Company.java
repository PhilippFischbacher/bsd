package DomainClasses;

public class Company {
	
	
	private int ID;//
	private String TelNr = "";//
	private String Name = "";//
	private String ContactPerson = "";//
	private String Email = "";//
	private String Address = "";//
	private String Password = ""; //UniqueIdent
	private Integer EventID = -1;//
	private Integer LocationplanID = -1;//

	public Company(String name, String email) {
		super();
		Name = name;
		Email = email;
	}
	
	
	
	public int getID() {
		return ID;
	}



	public void setID(int iD) {
		ID = iD;
	}



	public Integer getLocationplanID() {
		return LocationplanID;
	}



	public void setLocationplanID(Integer locationplanID) {
		LocationplanID = locationplanID;
	}



	public Company(String telNr, String name, String contactPerson, String email, String address, String password,
			Integer eventID, Integer locationplanID, int ID) {
		super();
		TelNr = telNr;
		Name = name;
		ContactPerson = contactPerson;
		Email = email;
		Address = address;
		Password = password;
		EventID = eventID;
		LocationplanID = locationplanID;
		this.ID=ID;
	}



	public String getJSON() {
		String CompJSON = "{";
		String newline = System.getProperty("line.separator");
		
		CompJSON += newline;
		CompJSON += "\"TelNr\": " + this.TelNr + ",";
		CompJSON += newline;
		CompJSON += "\"Name\": " + this.Name + ","; 
		CompJSON += newline;
		CompJSON += "\"ContactPerson\": " + this.ContactPerson + ",";
		CompJSON += newline;
		CompJSON += "\"Email\": " + this.Email + ",";
		CompJSON += newline;
		CompJSON += "\"Address\": " + this.Address + ",";
		CompJSON += newline;     
		CompJSON += "\"Password\": " + this.Password + ",";
		CompJSON += newline;
		CompJSON += "\"EventID\": " + Integer.toString(this.EventID) + ",";
		CompJSON += newline;
		CompJSON += "\"LocationplanID\": " + Integer.toString(this.LocationplanID);
		CompJSON += newline;
		CompJSON += "}";
		
		return CompJSON;
	}
	
	public String getTelNr() {
		return TelNr;
	}
	public void setTelNr(String telNr) {
		TelNr = telNr;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getContactPerson() {
		return ContactPerson;
	}
	public void setContactPerson(String contactPerson) {
		ContactPerson = contactPerson;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	
	public Integer getEventID() {
		return EventID;
	}

	public void setEventID(Integer eventID) {
		EventID = eventID;
	}


	
}
