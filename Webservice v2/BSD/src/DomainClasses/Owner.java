package DomainClasses;

public class Owner {

	private int ownerID;
	private String name;
	
	public void setOwnerID(int ownerID) {
		this.ownerID = ownerID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getOwnerID() {
		return ownerID;
	}
	
	
}
