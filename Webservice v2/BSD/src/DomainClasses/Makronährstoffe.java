package DomainClasses;

public class Makronährstoffe {

	private int ID;
	private int fette;
	private int kohlenhydrate;
	private int proteine ;
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public int getFette() {
		return fette;
	}
	public void setFette(int fette) {
		this.fette = fette;
	}
	public int getKohlenhydrate() {
		return kohlenhydrate;
	}
	public void setKohlenhydrate(int kohlenhydrate) {
		this.kohlenhydrate = kohlenhydrate;
	}
	public int getProteine() {
		return proteine;
	}
	public void setProteine(int proteine) {
		this.proteine = proteine;
	}
	
	public String toJson() {
		String CompJSON = "{";
		String newline = System.getProperty("line.separator");
		CompJSON +="\"ID\": " + this.ID + ",";
		CompJSON += newline;
		CompJSON +="\"fette\": " + this.fette + ",";
		CompJSON += newline;
		CompJSON +="\"kohlenhydrate\": " + this.kohlenhydrate + ",";
		CompJSON += newline;
		CompJSON +="\"proteine\": " + this.proteine + ",";
		CompJSON += newline;
		
		
		CompJSON +="}";
		return CompJSON;
	}
}
