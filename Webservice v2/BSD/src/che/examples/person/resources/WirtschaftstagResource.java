package che.examples.person.resources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import DALayer.*;
import DomainClasses.*;

// maps the resource to the URL persons
//@Path("/persons")
@Path("/wirtschaft")
public class WirtschaftstagResource {

	@Context
	UriInfo uriInfo;
	@Context
	Request request;

	List<Company> managedComps = new ArrayList<Company>();

	
	
	// http://localhost:8080/WWS/rest/wirtschaft/company
	@GET
	@Path("/company")
	@Produces(MediaType.TEXT_PLAIN)
	public String getCompany() {
		return "works";
	}

	@GET
	@Path("/get")
	@Produces(MediaType.TEXT_PLAIN)
	public String get() {
		return "LOL";
	}
	// http://localhost:8080/WWS/rest/wirtschaft/companies/abcinc
	/*
	 * @GET
	 * 
	 * @Path("/companies/{cname}")
	 * 
	 * @Produces(MediaType.TEXT_PLAIN) public String
	 * getCompanyByName(@PathParam("cname") String cname) { String res =
	 * "nicht vorhanden :C"; Company comp = new Company("Iwas Inc", "abc@abc.abc");
	 * Company comp2 = new Company("abcinc", "sig"); managedComps.add(comp);
	 * managedComps.add(comp2);
	 * 
	 * for (Company c : managedComps) { if (c != null && c.getName().equals(cname))
	 * { res = "Vorhanden.\n"; res += c.getJSON(); } }
	 * 
	 * return res; }
	 */

	// http://localhost:8080/WWS/rest/wirtschaft/companies/Firma01
	@GET
	@Path("/companies/{cname}")
	@Produces(MediaType.TEXT_PLAIN)
	public String getCompanyByName(@PathParam("cname") String cname) {
		String res = "company does not exist";

		try {
			Database.connect("Wirtschaftstag");
			DomainClasses.Company found = CompanyDAO.GetCompanyByName(cname);
			res = found.getJSON();
		} catch (Exception e) {

		}

		return res;
	}

	// http://localhost:8080/WWS/rest/wirtschaft/updateCompany/neuername/neuetel/neuecp/neuemail/neueaddr/neuespswd/3/3/1
	@GET
	@Path("/updateCompany/{cname}/{tel}/{cperson}/{mail}/{addr}/{psswd}/{evid}/{lpid}/{id}")
	@Produces(MediaType.TEXT_PLAIN)
	public String updateCompany(@PathParam("cname") String cname, @PathParam("tel") String tel,
			@PathParam("cperson") String cperson, @PathParam("mail") String mail, @PathParam("addr") String addr,
			@PathParam("psswd") String psswd, @PathParam("evid") Integer evid, @PathParam("lpid") Integer lpid,
			@PathParam("id") Integer id) {

		String res = "";
		
		try {
			Database.connect("Wirtschaftstag");
			Company c = new Company(tel, cname, cperson, mail, addr, psswd,evid, lpid,id);
			CompanyDAO.Update(c);
		}catch(Exception e) {
			res = "failed";
		}

		return res;
	}

	
	// http://localhost:8080/WWS/rest/wirtschaft/events

	// http://localhost:8080/WWS/rest/wirtschaft/companies/1
		@GET
		@Path("/companiesbyid/{evid}")
		@Produces(MediaType.TEXT_PLAIN)
		public String getCompanyByEvid(@PathParam("evid") Integer evid) {
			String res = "no companies for this event available";

			try {
				Database.connect("Wirtschaftstag");
				ArrayList<Company> comps = new ArrayList<Company>();
				comps = CompanyDAO.GetCompaniesOfEvent(evid);
				
				if(comps.size() > 0) {
					res = "";
				}
				
				for (DomainClasses.Company c : comps) {
					if (c != null) {
						res += c.getJSON();
						res += "\n";
					}
				}
			} catch (Exception e) {
				
			}
			return res;
		}
}
	


