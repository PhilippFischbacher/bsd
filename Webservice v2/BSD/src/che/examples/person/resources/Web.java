package che.examples.person.resources;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import DALayer.BetriebDAO;
import DALayer.Database;
import DALayer.GerichtDAO;
import DALayer.SpeisekartenDAO;
import DomainClasses.Betrieb;
import DomainClasses.Makronährstoffe;
import DomainClasses.Speisekarte;


@Path("/web")
public class Web {
	
	static {
		try {
			Database.connect("");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@GET
	@Path("/restaurant/")
	@Produces(MediaType.TEXT_PLAIN)
	public String getAllRestaurants() {
		String newline = System.getProperty("line.separator");
		ArrayList<Betrieb> betriebs= BetriebDAO.GetCompanies();

		String ret="["+newline;
		/*try {
			ret+=Database.conn.isClosed();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		for (Betrieb betrieb : betriebs) {
			ret+=betrieb.toJson()+newline;
			ret+=",";
		}
		ret=ret.substring(0, ret.length()-1);
		ret+="]"+newline;
		return ret;
	}
	
	@GET
	@Path("/restaurant/{id}")
	@Produces(MediaType.TEXT_PLAIN)
	public String getSpecificRestaurant(@PathParam("id") int id) {
		String ret="";
		String newline = System.getProperty("line.separator");
		ArrayList<Betrieb> betriebs= BetriebDAO.GetCompanies(id);
		for (Betrieb betrieb : betriebs) {
			ret+=betrieb.toJson()+newline;
			ret+=",";
		}
		ret=ret.substring(0, ret.length()-1);
		ret+="]"+newline;
		return ret;
	}
	
	@GET
	@Path("/speisekarte/{id}")
	@Produces(MediaType.TEXT_PLAIN)
	public String getGerichte(@PathParam("id") int id) {
		String ret="";
		String newline = System.getProperty("line.separator");
		ArrayList<Speisekarte> gerichts= SpeisekartenDAO.GetSpeisekarte(id);
		ret =gerichts.get(0).toJson();
		return ret;
	}
	
	@GET
	@Path("/gericht/{id}")
	@Produces(MediaType.TEXT_PLAIN)
	public String getGericht(@PathParam("id") int id) {
		String ret="";
		String newline = System.getProperty("line.separator");
		ArrayList<Makronährstoffe> gerichts= GerichtDAO.GetCompanies(id);
		ret =gerichts.get(0).toJson();
		return ret;
	}
	
}
