package DALayer;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import DALayer.PropertyFileLoader;

public class Database {

	public static Connection conn;
	
	public static Boolean connect(String database) throws SQLException, IOException {
		System.out.println("TryingToConnect...\r\n");
		Path currentRelativePath = Paths.get("");
		Statement smt;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		conn= DriverManager.getConnection("jdbc:oracle:thin:@//212.152.179.117:1521/ora11g", PropertyFileLoader.LoadPropertyFile("/opt/apache-tomcat/webapps/oracleextern.conf"));
		smt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
		//.execute("Use "+database+";");
		if(!conn.isClosed()) {
			System.out.println("Connected");
			return true;
			}
		else
		{
			System.out.println("Failed To Connect");
			return false;
		}
	}
	
	public static ResultSet StateInsertOrUpdate(String query) throws SQLException {
		if (conn!=null) {
			Statement smt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			smt.executeUpdate(query,Statement.RETURN_GENERATED_KEYS);
			return smt.getGeneratedKeys();
		}
		return null;
	}
	
	public static ResultSet StateSelect(String query) throws SQLException {
		ResultSet ret=null;
		if (conn!=null) {
			Statement smt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			ret= smt.executeQuery(query);
		}
		return ret;
		
	}
}
