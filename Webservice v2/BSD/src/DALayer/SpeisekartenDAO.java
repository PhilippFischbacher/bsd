package DALayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import DomainClasses.Gericht;
import DomainClasses.Speisekarte;

public class SpeisekartenDAO {
	
	private static ArrayList<Speisekarte> DummyData= new ArrayList<Speisekarte>();
	
	static {
		Speisekarte sp= new Speisekarte();
		DummyData.add(sp);
	}

	public static ArrayList<Speisekarte> GetSpeisekarte(int id) {
		ResultSet set=null;
		ArrayList<Speisekarte> companys= new ArrayList<Speisekarte>();
		try {
			 set=Database.StateSelect("Select s.SPEISEKARTENID as SID, g.GERICHTID as GID, g.BEZEICHNUNG as GB, "
			 		+ "g.PREIS as Preis, g.\"MAKRONÄHRSTOFFE\" as Makro from SPEISEKARTE s join Gericht g on "
			 		+ "g.SPEISEKARTENID=s.speisekartenID  where s.SPEISEKARTENID=4\n" + 
			 		"");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(set==null) {
			companys= DummyData;
		}
		else {
			try {
				while(set.next()) {
					Speisekarte sp= new Speisekarte();
					sp.setID(set.getInt(1));
					
					Gericht g = new Gericht();
					g.setID(set.getInt(2));
					g.setBezeichnung(set.getString(3));
					g.setPreis(set.getInt(4));
					g.setMakronaehrstoffe(set.getInt(5));
					
					sp.getGerichts().add(g);
					
					companys.add(sp);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("SIzE: "+companys.size());
		return companys;
	}
}
