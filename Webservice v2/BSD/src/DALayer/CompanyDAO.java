package DALayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import DALayer.Database;
import DomainClasses.*;

public class CompanyDAO {
	private static ArrayList<Company> DummyData= new ArrayList<Company>();
	
	static {
		Company c= new Company("DUMMY","DUMMY","DUMMY","DUMMY","DUMMY","DUMMY",1,1,1);
		DummyData.add(c);
	}
	
	public static ArrayList<Company> GetCompaniesOfEvent( int EventID) {
		ResultSet set=null;
		ArrayList<Company> companys= new ArrayList<Company>();
		try {
			 set=Database.StateSelect("SELECT * FROM `Company` WHERE `Event_ID`="+EventID);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(set==null) {
			companys= DummyData;
		}
		else {
			try {
				while(set.next()) {
					companys.add(new Company(set.getString(7),set.getString(3),set.getString(5),set.getString(6),set.getString(4),
							set.getString(10), set.getInt(2), set.getInt(8),set.getInt(1)));
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return companys;
	}
	
	public static Company GetCompanyByID(int ID, int EventID) {
		ResultSet set=null;
		ArrayList<Company> companys= new ArrayList<Company>();
		try {
			 set=Database.StateSelect("SELECT * FROM `Company` WHERE `ID`="+ID+" AND `Event_ID`="+EventID);
			 //System.out.println("SELECT * FROM `Company` WHERE `ID`="+ID+" AND `Event_ID`="+EventID);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(set==null) {
			//System.out.println("Dummy");
			companys= DummyData;
		}
		else {
			try {
				while(set.next()) {
					companys.add(new Company(set.getString(7),set.getString(3),set.getString(5),set.getString(6),set.getString(4),
							set.getString(10), set.getInt(2), set.getInt(8),set.getInt(1)));
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(companys.size()<1)
			companys= DummyData;
		return companys.get(0);
	}
	
	public static Company GetCompanyByIdentifier(String ident) {
		ResultSet set=null;
		ArrayList<Company> companys= new ArrayList<Company>();
		try {
			 set=Database.StateSelect("SELECT * FROM `Company` WHERE `UniqueIdentifier` LIKE('"+ident+"');");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(set==null) {
			companys= DummyData;
		}
		else {
			try {
				while(set.next()) {
					companys.add(new Company(set.getString(7),set.getString(3),set.getString(5),set.getString(6),set.getString(4),
							set.getString(10), set.getInt(2), set.getInt(8),set.getInt(1)));
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(companys.size()<1)
			companys= DummyData;
		return companys.get(0);
	}
	
	public static Company GetCompanyByName(String name) {
		ResultSet set=null;
		ArrayList<Company> companys= new ArrayList<Company>();
		try {
			 set=Database.StateSelect("SELECT * FROM `Company` WHERE `Name` LIKE('"+name+"');");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(set==null) {
			companys= DummyData;
		}
		else {
			try {
				while(set.next()) {
					companys.add(new Company(set.getString(7),set.getString(3),set.getString(5),set.getString(6),set.getString(4),
							set.getString(10), set.getInt(2), set.getInt(8),set.getInt(1)));
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(companys.size()<1)
			companys= DummyData;
		return companys.get(0);
	}
	
	public static int InsertCompany(Company toInsert) {
		
		ResultSet set=null;
		try {
			set=Database.StateInsertOrUpdate("INSERT INTO `Company`(`ID`, `Event_ID`, `Name`, `Address`, `Contact_Person`, `Email`, "
			 		+ "`Phone-Number`, `Locationplan_Nr`, `Locationplan_Event_ID`, `UniqueIdentifier`) "
			 		+ "VALUES (NULL,'"+toInsert.getEventID()+"','"+toInsert.getName()+"','"+toInsert.getAddress()+"','"+toInsert.getContactPerson()+"','"
			 				+ toInsert.getEmail()+"','"+toInsert.getTelNr()+"','"+toInsert.getLocationplanID()+"','"+toInsert.getEventID()+"','"+toInsert.getPassword()+"');");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			set.next();
			return set.getInt(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
		
	} 
	public static void Update(Company toUpdate) {
		
		ResultSet set=null;
		try {
			set=Database.StateInsertOrUpdate("UPDATE `Company` SET `Event_ID`='"+toUpdate.getEventID()+"',"
					+ "`Name`='"+toUpdate.getName()+"',`Address`='"+toUpdate.getAddress()+"',`Contact_Person`='"
					+toUpdate.getContactPerson()+"',`Email`='"+toUpdate.getEmail()+"',"
					+ "`Phone-Number`='"+toUpdate.getTelNr()+"',`Locationplan_Nr`='"+toUpdate.getLocationplanID()+
					"',`Locationplan_Event_ID`='"+toUpdate.getEventID()+"',"
					+ "`UniqueIdentifier`='"+toUpdate.getPassword()+"' WHERE `ID` Like('"+toUpdate.getID()+"')");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	} 
	
}
