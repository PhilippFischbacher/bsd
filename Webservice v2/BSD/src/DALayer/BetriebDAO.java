package DALayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import DomainClasses.Betrieb;

public class BetriebDAO {
	private static ArrayList<Betrieb> DummyData= new ArrayList<Betrieb>();
	
	static {
		Betrieb c= new Betrieb();
		DummyData.add(c);
	}
	
	public static ArrayList<Betrieb> GetCompanies() {
		ResultSet set=null;
		ArrayList<Betrieb> companys= new ArrayList<Betrieb>();
		try {
			 set=Database.StateSelect("Select BETRIEBID as ID, LAT as LAT, LNG as LNG, NAME as Name, SPEISEKARTE as S, OWNER o from Betrieb");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(set==null) {
			companys= DummyData;
		}
		else {
			try {
				while(set.next()) {
					Betrieb b= new Betrieb();
					b.setBetriebID(set.getInt(1));
					b.setLat(set.getString(2));
					b.setLng(set.getString(3));
					b.setName(set.getString(4));
					b.setOwner(set.getInt(6));
					b.setSpeisekarte(set.getInt(5));
					companys.add(b);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("SIzE: "+companys.size());
		return companys;
	}
	
	public static ArrayList<Betrieb> GetCompanies(int id) {
		ResultSet set=null;
		ArrayList<Betrieb> companys= new ArrayList<Betrieb>();
		try {
			 set=Database.StateSelect("Select BETRIEBID as ID, LAT as LAT, LNG as LNG, NAME as Name, SPEISEKARTE as S, OWNER o from Betrieb where BETRIEBID="+id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(set==null) {
			companys= DummyData;
		}
		else {
			try {
				while(set.next()) {
					Betrieb b= new Betrieb();
					b.setBetriebID(set.getInt(1));
					b.setLat(set.getString(2));
					b.setLng(set.getString(3));
					b.setName(set.getString(4));
					b.setOwner(set.getInt(6));
					b.setSpeisekarte(set.getInt(5));
					companys.add(b);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("SIzE: "+companys.size());
		return companys;
	}
}
