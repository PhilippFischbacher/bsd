package DALayer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyFileLoader {

	public static Properties LoadPropertyFile(String filepath) throws IOException{
		Properties props = new Properties();
		InputStream in = new FileInputStream(new File(filepath));
		props.load(in);
		return props;
	}
}
