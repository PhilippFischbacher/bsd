package DALayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import DomainClasses.Makronährstoffe;

public class GerichtDAO {

	private static ArrayList<Makronährstoffe> DummyData= new ArrayList<Makronährstoffe>();
	
	
	static {
		Makronährstoffe c= new Makronährstoffe();
		DummyData.add(c);
	}
	
	public static ArrayList<Makronährstoffe> GetCompanies(int id) {
		ResultSet set=null;
		ArrayList<Makronährstoffe> companys= new ArrayList<Makronährstoffe>();
		try {
			 set=Database.StateSelect("Select m.FETTE as f, m.KOHLENHYDRATE as k, m.PROTEINE as p, m.\"MAKRONÄHRSTOFFEID\" as ID from GERICHT g join \"MAKRONÄHRSTOFFE\" m on g.\"MAKRONÄHRSTOFFE\"=m.\"MAKRONÄHRSTOFFEID\" where g.GERICHTID="+id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(set==null) {
			companys= DummyData;
		}
		else {
			try {
				while(set.next()) {
					Makronährstoffe m = new Makronährstoffe();
					m.setFette(set.getInt(1));
					m.setKohlenhydrate(set.getInt(2));
					m.setProteine(set.getInt(3));
					m.setID(set.getInt(4));
					companys.add(m);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("SIzE: "+companys.size());
		return companys;
	}
}
