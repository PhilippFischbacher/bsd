--DummyData

--Owner 
insert into Owner values(1,'Owner1', 121);
insert into Owner values(2,'Owner2', 122);
insert into Owner values(3,'Owner3', 123);
insert into Owner values(4,'Owner4', 124);
insert into Owner values(5,'Owner5', 125);

--Speisekarte
insert into SPEISEKARTE values(1);
insert into SPEISEKARTE values(2);
insert into SPEISEKARTE values(3);
insert into SPEISEKARTE values(4);

--Betrieb
insert into BETRIEB values(1,'46.614106','13.845081','Betrieb1',1,1);
insert into BETRIEB values(2,'46.624106','13.845091','Betrieb2',2,2);
insert into BETRIEB values(3,'46.634106','13.845011','Betrieb3',3,3);
insert into BETRIEB values(4,'46.644106','13.845031','Betrieb4',4,4);
insert into BETRIEB values(5,'46.654106','13.841061','Betrieb5',4,5);

--Makronährstoffe
insert into "MAKRONÄHRSTOFFE" VALUES(1,1,1,1);
insert into "MAKRONÄHRSTOFFE" VALUES(2,2,2,2);
insert into "MAKRONÄHRSTOFFE" VALUES(3,3,3,3);


--Allergene
insert into ALLERGENE VALUES(1,'Alergen1');
insert into ALLERGENE VALUES(2,'Alergen2');

--Gericht
--insert into GERICHT values(GERICHTID,BEZEICHNUNG,"MAKRONÄHRSTOFFE",SPEISEKARTENID
insert into GERICHT values(1,'Gericht1',12,1,1);
insert into GERICHT values(2,'Gericht2',12,2,2);
insert into GERICHT values(3,'Gericht3',11,3,3);
insert into GERICHT values(4,'Gericht3',18,3,4);
insert into GERICHT values(5,'Gericht3',9,3,4);

--GerichtAllergene
--insert into GERICHTALLERGENE VALUES(GERICHTID,ALLERGENID)
insert into GERICHTALLERGENE values(1,1);
insert into GERICHTALLERGENE values(2,1);
insert into GERICHTALLERGENE values(3,2);
insert into GERICHTALLERGENE values(4,2);
insert into GERICHTALLERGENE values(5,2);